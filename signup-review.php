<?php
// Start a session
session_start();

// Handles requests made to this page
require "form-controller.php";

?>

<!DOCTYPE html>
<html>
<head>
<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
    <title>Creditsafe Sign-up Form</title>
    
</head>
<body class="landingpage">

<!-- Notification-bar -->
<div class="fixed-nav-bar"><?=$msg?></div>
<br><br>

<div class="container-fluid">
		<div class="row">
			<div class="col-md">
            <h2><i class="far fa-building"></i>&nbsp;Company</h2>
            <br>
				<div class="row">

					<div class="col-md">        
                    <h5>Name:</h5><p><?=$json_output['company']['name']?></p>
                    <h5>Email:</h5><p><?=$json_output['company']['email']?></p>
                    <h5>Address:</h5><p><?=$json_output['company']['address']?></p>
                    <h5>House number:</h5><p><?=$json_output['company']['phone']?></p>                    
                    </div>

                    <div class="col-md">
                    <h5>Postal code:</h5><p><?=$json_output['company']['postalcode']?></p>
                    <h5>City:</h5><p><?=$json_output['company']['city']?></p>
                    <h5>Country:</h5><p><?=$json_output['company']['country']?></p>
                    <h5>Phone number:</h5><p><?=$json_output['company']['phone']?></p>
                    </div>
                    
                    <div class="col-md">
                    <h5>IBAN:</h5><p><?=$json_output['company']['iban']?></p>
                    <h5>KvK:</h5><p><?=$json_output['company']['kvk']?></p>
                    <h5>VAT:</h5><p><?=$json_output['company']['vat']?></p>
                    </div>
                    </div>

                    <hr>
                    <br>
                    <div class="row">
                    <div class="col-md">
                    <h2><i class="far fa-user"></i>&nbsp;Main User</h2>
                    <br>
                    <h5>First name:</h5><p><?=$json_output['main-user']['firstname']?></p>
                    <h5>Last name:</h5><p><?=$json_output['main-user']['lastname']?></p>
                    <h5>Email:</h5><p><?=$json_output['main-user']['email']?></p>
                    <h5>Phone number:</h5><p><?=$json_output['main-user']['phone']?></p>
                    </div>
                    
                    <div class="col-md">
                    <h2><i class="far fa-user"></i>&nbsp;Second User</h2>
                    <br>
                    <h5>First name:</h5><p><?=$json_output['second-user']['firstname']?></p>
                    <h5>Last name:</h5><p><?=$json_output['second-user']['lastname']?></p>
                    <h5>Email:</h5><p><?=$json_output['second-user']['email']?></p>
                    <h5>Phone number:</h5><p><?=$json_output['second-user']['phone']?></p>
                    </div>
                    </div>
                    
                    <hr>

                    <div class="row">

                    <div class="col-md">
                    <br>
                    <img width="200" src="img/creditsafe.png">
                    <br><br>
                    <h5>Username:</h5><p><?=$json_output['credit-safe']['username']?></p>
                    <h5>Password:</h5><p><?=$json_output['credit-safe']['password']?></p>
                    </div>

                      <div class="col-md">
                      <br>
                      <h2><i class="fas fa-cube"></i>&nbsp;Bundle</h2>
                      <p><?=$json_output['credit-safe']['bundle']?> debtors max</p>
                        </div>

                        <div class="col-md">
                        <br>
                        <h2><i class="fas fa-cogs"></i>&nbsp;Customer matching</h2>
                        <p><?=$customer_matching?></p>
                        </div>
                        </div>
                            <hr>
                          <div class="row">
                        <div class="col-md">
                        <br>
                        <h2><i class="fas fa-download"></i>&nbsp;Import Method</h2>
                        
                        <p><?=$json_output['import']?></p>
                    <br>
                    <h2><i class="fas fa-check-circle"></i>&nbsp;Import method checkbox</h2>
                    <p><?=$json_output['dbasics']['installed']?></p>
                        </div>
                        </div>
                    <hr>
                    <div class="row">
                    <div class="col-md">
                    <br>
                    <h2><i class="fas fa-info-circle"></i>&nbsp;Additional Information</h2>
                    <br>
                    <?=$json_output['additional-info']?>
                    </div>
                    </div>

                    <br>
                    <hr>
                    <br>

                    <div class="col-md">
                    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
                        <button type="submit" name="back"><i class="fas fa-undo"></i>&nbsp;&nbsp;Go back</button>
                        <button type="submit" name="confirm"><i class="fas fa-clipboard-check"></i>&nbsp;&nbsp;Confirm</button>
                        </form>
                     <br>
                    </div>

                </div>
			</div>
		</div>
    </div>
    <!-- Notification bar -->
    <script>signupReview()</script>
</body>
</html>