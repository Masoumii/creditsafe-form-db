<?php
session_start();

// If user submits the form
require_once "signup.php";

// If user comes back to edit form
require_once "form-filler.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
	<title>Creditsafe Sign-up Form</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md">

				<h2><i class="far fa-building"></i>&nbsp;Company</h2>
				<br>
				<div class="row">
					<div class="col-md">

						<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
							<input alt="Company name" name="company-name" value="<?=$c_name?>" type="text" placeholder="Company Name" required> 
							<input name="company-email" value="<?=$c_email?>" type="email" placeholder="Email Address" required>
							<input name="company-address" value="<?=$c_address?>" type="text" placeholder="Visit Address" required> 
							<input name="house-number" value="<?=$c_house_number?>" type="tel" placeholder="House number" required>
							<input name="company-postalcode" value="<?=$c_postal_code?>" type="text" placeholder="Postal code" required> 
                    </div>
                    
					<div class="col-md">
						<input name="company-city" value="<?=$c_city?>" type="text" placeholder="City" required>
						<input name="company-country" value="<?=$c_country?>" type="text" placeholder="Country" required> 
						<input name="company-phone" value="<?=$c_number?>" type="tel" placeholder="Phone number" required>
                    </div>
                    
					<div class="col-md">
						<input name="iban" value="<?=$c_iban?>" type="text" placeholder="IBAN" required> 
						<input name="kvk" value="<?=$c_kvk?>" type="text" placeholder="KvK" required>
                        <input name="vat" value="<?=$c_vat?>" type="text" placeholder="VAT" required>
                    </div>
                </div>
				<hr>
				<div class="row">
					<div class="col-md">
						<br>
					<h2><i class="far fa-user"></i>&nbsp;Main User</h2>
					<br>
                        <input name="main-firstname" value="<?=$user1_firstname?>" type="text" placeholder="First Name" required>
                        <input name="main-lastname" value="<?=$user1_lastname?>"  type="text" placeholder="Last Name" required>
                        <input name="main-email" value="<?=$user1_email?>" type="email" placeholder="Email Address" required>
                        <input name="main-phone" value="<?=$user1_phone?>" type="tel" placeholder="Phone number" required>
                    </div>
                    
					<div class="col-md">
					<br>
						<h2><i class="far fa-user"></i>&nbsp;Second User</h2>
						<br>
                        <input name="sec-firstname" value="<?=$user2_firstname?>" type="text" placeholder="First Name">
                        <input name="sec-lastname" value="<?=$user2_lastname?>" type="text" placeholder="Last Name">
                        <input name="sec-email" value="<?=$user2_email?>" type="email" placeholder="Email Address">
                        <input name="sec-phone" value="<?=$user2_phone?>" type="tel" placeholder="Phone number">
					</div>
                </div>
				<hr>
				<div class="row">
					<div class="col-md">
					<br>
						<img alt="Creditsafe" width="200" src="img/creditsafe.png">
						<br>
                        <input name="username" value="<?=$username?>" type="text" placeholder="Username" required>
                        <input name="password" value="<?=$password?>" type="password" placeholder="Password" required>
                    </div>
                    
					<div class="col-md">
					<br>
					<h2><i class="fas fa-cube"></i>&nbsp;Bundle</h2>
						<br>
						<input name="bundle" type="radio" class="debtors" value="200" required  <?php if(isset($bundle) && $bundle == "200"){print " checked=\"checked\"";} ?>> 200 debtors max<br>
						<input name="bundle" type="radio" class="debtors" value="500" <?php if(isset($bundle) && $bundle == "500"){print " checked=\"checked\"";} ?>> 500 debtors max<br>
						<input name="bundle" type="radio" class="debtors" value="1000" <?php if(isset($bundle) && $bundle == "1000"){print " checked=\"checked\"";} ?>> 1000 debtors max<br>
                    </div>
                    
					<div class="col-md">
					<br>
						<h2><i class="fas fa-cogs"></i>&nbsp;Customer matching</h2>
						<br>
                        <div class="form-check"></div>
                        <label for="manual">Manual</label>
						<input name="cmatching" type="radio" class="form-check-label" value="manual" required <?php if(isset($customer_matching) && $customer_matching === "manual"){print " checked=\"checked\"";} ?>><br>
						
                        <label for="batch">Batch</label> 
                        <input name="cmatching" type="radio" class="form-check-label" value="batch" <?php if(isset($customer_matching) && $customer_matching === "batch"){print " checked=\"checked\"";} ?>>
                    </div>
                </div>
				<hr>
				<div class="row">
					<div class="col-md">
					<br>
						<h2><i class="fas fa-download"></i>&nbsp;Import Method</h2>
						<div class="col-sm dbasics"> 
							<input name="importmethod" type="radio" class="dbasics-radio" value="dbasics" required <?php if(isset($import_method) && $import_method === "dbasics"){print " checked=\"checked\"";} ?>>
							<label for="dbasics-radio">DBasics</label>
                        </div>
                        
						<div class="col-sm csv">
							<input name="importmethod" type="radio" class="csv-radio" value="csv" <?php if(isset($import_method) && $import_method === "csv"){print " checked=\"checked\"";} ?>>
                            <label for="dbasics-radio">CSV</label> 
                        </div>
					</div>
                </div>
                

				<div class="dbasics-col row" id="dbasics">
					<div class="col-md">
					<br>
                        <h2>DBasics Addtional Questions</h2>
                        <input name="dbasics-checkbox" value="dbasics-checkbox" <?php if(isset($import_method) && $import_method === "dbasics" && isset($checked)) echo 'checked="checked"'; ?> type="checkbox" id="dbasics-option">
                        <label for="dbasics-option">D-Basics is already installed at our company</label>
					</div>
                </div>
				<div class="csv-col row" id="csv">
					<div class="col-md">
					<br>
						<h2>CSV Import Settings</h2>
						<input name="csv-checkbox" value="csv-checkbox" <?php if(isset($import_method) && $import_method === "csv" && isset($checked)) echo 'checked="checked"'; ?> type="checkbox" id="csv-option">
                        <label for="csv-option">I already know the format of the CSV file</label>
					</div>
                </div>

				<hr>
				<div class="row">
					<div class="col-md">
					<br>
					<h2><i class="fas fa-info-circle"></i>&nbsp;Additional Information&nbsp;</h2>
						<textarea name="info" class="additional-information"></textarea>
					</div>
                </div>
				
				<button name="signup" type="submit"><i class="fas fa-paper-plane"></i>&nbsp; Send Form</button>
			</form>
			</div>
		</div>
    </div>
</body>
</html>