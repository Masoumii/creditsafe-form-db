<?php
if (isset($_POST['signup'])) {

	// Empty variables to be used by conditional statements below
	$checkbox = "";
	$checkbox_value = "";

	// Assign import method choice to checkbox variable
	$checkbox = $_POST['importmethod']; // Holds either 'dbasics' or 'csv' as value

	// Check if checkbox has been selected or not and return value
	// Dbasics choice
	if ( isset($_POST['dbasics-checkbox']) || isset($_POST['csv-checkbox'])) {
		$checkbox_value = true;
	}

	// Extract all the entered values, insert them into an array to be used as JSON and inserted into database
	$creditsafe_form[] = array(

		// Company details
		'company' => array(
			'name' => $_POST['company-name'],
			'email' => $_POST['company-email'],
			'address' => $_POST['company-address'],
			'house_number' => $_POST['house-number'],
			'postalcode' => $_POST['company-postalcode'],
			'city' => $_POST['company-city'],
			'country' => $_POST['company-country'],
			'phone' => $_POST['company-phone'],
			'iban' => $_POST['iban'],
			'kvk' => $_POST['kvk'],
			'vat' => $_POST['vat'],
		) ,

		// Main user details
		'main-user' => array(
			'firstname' => $_POST['main-firstname'],
			'lastname' => $_POST['main-lastname'],
			'email' => $_POST['main-email'],
			'phone' => $_POST['main-phone'],
		) ,

		// Second user details
		'second-user' => array(
			'firstname' => $_POST['sec-firstname'],
			'lastname' => $_POST['sec-lastname'],
			'email' => $_POST['sec-email'],
			'phone' => $_POST['sec-phone'],
		) ,

		// User account details
		'credit-safe' => array(
			'username' => $_POST['username'],
			'password' => $_POST['password'],
			'bundle' => $_POST['bundle'],
			'matching' => $_POST['cmatching'],
		) ,

		// Additional information
		'additional-info' => $_POST['info'],

		// Import method
		'import' => $_POST['importmethod'],

		// Dbasics
		'dbasics' => array(
			// Dbasics/CSV checkbox
			'installed' => $checkbox_value,
		),
	);

	// Convert values to JSON and remove the brackets
	$json = str_replace(array('[',']') , '', json_encode($creditsafe_form));

	// Save the JSON in a session variable to be used on other pages
	$_SESSION['json'] = $json;
	header("Location: signup-review.php");
}
?>