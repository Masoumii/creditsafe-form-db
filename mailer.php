<?php

require("phpmailer/class.phpmailer.php");
$mail = new PHPMailer();

// Security settings
$SMTPHost = 'localhost';
$SMTPHostBackup = '';
$SMTPPort = 25;

// Mail settings
$mail->IsSMTP();          // set mailer to use SMTP
$mail->Host = $SMTPHost;  // specify main and backup server
$mail->Port = $SMTPPort;
$mail->SMTPSecure = ""; // secure transfer enabled REQUIRED for Gmail
$mail->SMTPAuth = false;  // turn on SMTP authentication
$mail->Username = "";     // SMTP username
$mail->Password = "";     // SMTP password

$mail->From = $mailSender;
$mail->FromName = $mailSenderName;

$mail->SMTPDebug = 2; // debugging: 1 = errors and messages, 2 = messages only

$mail->AddAddress($mailReceiver, $mailReceiverName);
//$mail->AddAddress($mailReceiver2, $mailReceiverName2); // name is optional
$mail->AddReplyTo("masoumi@s4financials.com", "Information");

$mail->WordWrap = 50;                                 // set word wrap to 50 characters    // optional name
$mail->IsHTML(true);                                  // set email format to HTML

$mail->Subject = $mailSubject;
$mail->Body    = $mailBody;
$mail->AltBody = $mailAltBody;

if(!$mail->Send())
{
   echo "Message could not be sent. 
";
   echo "Mailer Error: " . $mail->ErrorInfo;
   exit;
}

echo "Message has been sent";

?>