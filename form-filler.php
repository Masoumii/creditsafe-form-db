<?php
// If user clicked on back button
if(isset($_SESSION['back']) && isset($_SESSION['json'])){
	
    // Decode the JSON from the session and assign decoded response to variable
    $json_output = json_decode($_SESSION['json'], true);

    // Prefill the input fields
	// Company details
	$c_name = $json_output['company']['name'];
	$c_email = $json_output['company']['email'];
	$c_address = $json_output['company']['address'];
	$c_house_number = $json_output['company']['house_number'];
	$c_postal_code = $json_output['company']['postalcode'];
	$c_city = $json_output['company']['city'];
	$c_country = $json_output['company']['country'];
	$c_number = $json_output['company']['phone'];
	$c_iban = $json_output['company']['iban'];
	$c_kvk = $json_output['company']['kvk'];
	$c_vat = $json_output['company']['vat'];


	// Main-user details
	$user1_firstname = $json_output['main-user']['firstname'];
	$user1_lastname = $json_output['main-user']['lastname'];
	$user1_email = $json_output['main-user']['email'];
	$user1_phone = $json_output['main-user']['phone'];

	// Second-user details
	$user2_firstname = $json_output['second-user']['firstname'];
	$user2_lastname = $json_output['second-user']['lastname'];
	$user2_email = $json_output['second-user']['email'];
	$user2_phone = $json_output['second-user']['phone'];

	// Creditsafe
	$username = $json_output['credit-safe']['username'];
	$password = $json_output['credit-safe']['password'];
	$bundle = $json_output['credit-safe']['bundle'];
	$customer_matching = $json_output['credit-safe']['matching'];

	// Additional information
	$additional_info = $json_output['additional-info'];

	// Import method & checkbox
	$import_method = $json_output['import'];
	$checked = $json_output['dbasics']['installed'];

// Default
}else{
	// Empty values
	$c_name = "";
	$c_email = "";
	$c_address = "";
	$c_house_number = "";
	$c_postal_code = "";
	$c_city = "";
	$c_country = "";
	$c_number = "";
	$c_iban = "";
	$c_kvk = "";
	$c_vat = "";
	$user1_firstname = "";
	$user1_lastname = "";
	$user1_email = "";
	$user1_phone = "";
	$user2_firstname = "";
	$user2_lastname = "";
	$user2_email = "";
	$user2_phone = "";
	$username = "";
	$password = "";
	$bundle = "";
	$customer_matching = "";
	$import_method = "";
	$checked = "";
	$additional_info = "";
}
?>