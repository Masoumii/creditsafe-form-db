<?php
class DatabaseConnection
{
    
    private static $db;
    
    //voorkom meerdere "instances"
    private function __construct(){}
    
    // Static methode om connectie functie te krijgen
    public static function getConnection()
    {
        
        //Als er geen connectie objecten zijn, maak dan een nieuwe
        if (!self::$db) :
            
            // Nieuwe connectie object
            self::$db = self::openNewConnection();
		endif;
        
        //return de connectie
        return self::$db;
    }
    
    // connectie openen
    private static function openNewConnection()
    {
        
        $connection = null;
        
        try {
            // Database gegevens vereist om in te loggen
            $connection = new PDO('mysql:host=37.97.134.136;port=3306;dbname=setuptool', 'setuptool', 'a7s8d9f4g5h6j1k2l3');
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
        }
            // Fouten opvangen
        catch (PDOException $e) {
            
            echo "Fout met verbinden: " . $e->getMessage();
        }
        
        return $connection;
    }

}
?>