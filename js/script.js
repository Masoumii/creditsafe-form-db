// Custom functions

// Notification bar on the review page
function signupReview(){
	$(".fixed-nav-bar").hide().delay(750).slideDown(1000);
}

// Animated text & notification-bar on signup-completion page
function signupCompleted(){

   $(".fixed-nav-bar").hide().text('Form has been submitted!').slideDown(1000).delay(4000).slideUp(1000);
   $(".display-1").hide().delay(1000).fadeIn(1000).delay(5000).fadeOut(1500);
   $(".display-3").hide().delay(1500).fadeIn(1000).delay(4500).fadeOut(1500);
   $(".display-4").hide().delay(2000).fadeIn(1000).delay(4000).fadeOut(1500);
   $(".dashboard-img").hide().delay(2500).fadeIn(1000).delay(3500).fadeOut(1500);
   
   // Send user back to Homepage
   setTimeout(function(){
	   window.location.replace("http://creditsafesoftware.com");
	}, 10000);
}

//jQuery functions
$(document).ready(function() {

	// Fade in the page on load
	$("body").hide().fadeIn(1000);

	// Show / Hide div based on radio button choice
	$("input[class='dbasics-radio']").on("click", function() {
		$("#csv").hide();
		$("#dbasics").show();
	});

	$("input[class='csv-radio']").on("click", function() {
		$("#dbasics").hide();
		$("#csv").show();
	});
});