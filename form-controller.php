<?php
// Grab JSON data attached to a SESSION variable from previous page and decode it to be used in the body
$json_output = json_decode($_SESSION['json'], true);


// Change text values based on conditional statements without changing the back-end value

// If D-Basics chosen as import method & it's checkbox is checked
if($json_output['import'] === "dbasics" && $json_output['dbasics']['installed'] === "true"){
    $checked = "Yes, D-Basics is already installed at our company";
}

// If D-Basics chosen as import method & it's checkbox is not checked
if($json_output['import'] === "dbasics" && $json_output['dbasics']['installed'] === ""){
    $checked = "No, D-Basics is not installed at our company";
}

// If CSV chosen as import method & it's checkbox is checked
if($json_output['import'] === "csv" && $json_output['dbasics']['installed'] === "true"){
    $checked = "Yes, I already know the format of the CSV file";
}

// If CSV chosen as import method & it's checkbox is not checked
if($json_output['import'] === "csv" && $json_output['dbasics']['installed'] === ""){
    $checked = "No, I do not know the format of the CSV file";
}

// If Import method is CSV
if($json_output['import'] === "csv"){
    $import_method = "CSV";
}

// If import method is D-Basics
if($json_output['import'] === "dbasics"){
    $import_method = "D-Basics";
}

// If customer matching is Batch
if($json_output['credit-safe']['matching'] === "batch"){
    $customer_matching = "Batch";
}

// If customer matching is Batch
if($json_output['credit-safe']['matching'] === "manual"){
    $customer_matching = "Manual";
}

// Standard notification-bar message
$msg = "You have submitted the following information. Please <span style='text-decoration:underline'>review your application</span> before confirming it below...";

// If user has confirmed the form application
if(isset($_POST['confirm'])){

    // Insert JSON & source into database
	require "dbconfig.php";
	$conn = DatabaseConnection::getConnection();
	$q = " INSERT INTO `settings` (json, source) VALUES ('".$_SESSION['json']."', 'json') ";
	$stmt = $conn->prepare($q);
    
    // If data succesfully entered into database
    if ($stmt->execute()) {

        // Unset and destroy the sensitive form information when the sign-up process is completed
        session_unset();
        session_destroy();



        // Send mail when form is submitted
        // Assign sender details
        $mailSender = "masoumiprojects7@gmail.com";
        $mailSenderName = "Mohammad";

        // Assign receiver details
        $mailReceiver = "masoumiprojects7@gmail.com";
        $mailReceiverName = "Mohammad";
        $mailReceiver2 = "jansen@s4financials.com";
        $mailReceiverName2 = "Jeroen Jansen";

       /* $recipients = array(
            'masoumiprojects7@gmail.com' => 'Mohammad',
            'masoumi@s4financials.com' => 'Mohammad',
         );

         foreach($recipients as $email => $name)
         {
            $mail->AddAddress($email, $name);
         }
         */

        // Assign mail subject, body & alt body
        $mailSubject = "Creditsafe form submitted!";
        $mailBody = "Hello ".$mailReceiverName.", A creditsafe form has been submitted by ". $json_output['company']['name'];
        $mailAltBody = null; // Non-HTML text

        // Finally, send the mail
        require "mailer.php";
      
        // Send user to the completion landingpage
       header("Location: signup-completed.php");

     }
     else {
         $msg = "Form could not be submitted. Please contact the administrator";

         // Send mail to admin with an error message
         $mailSender = "masoumiprojects7@gmail.com";
         $mailSenderName = "Mohammad";
         $mailReceiver = "masoumiprojects7@gmail.com";
         $mailReceiverName = "Mohammad";
         $mailReceiver2 = null; // accepts email-address
         $mailSubject = "The form could not be submitted.";
         $mailBody = "The form could not be submitted. <br> Please check the database";
         $mailAltBody = null; // Non-HTML text
     }
}
    // If user wants to go back to change values
    if(isset($_POST['back'])){
    
    $_SESSION['back'] = true;
    header("Location: index.php");
    }else{
        $_SESSION['back'] = "";
    }
?>